#
# Demonstrates the implementation of a simple hash table in Python.
#

class BucketItem:
    """Defines an item that lives in a bucket.  Note that we store both the key and the value."""
    def __init__(self, key, value):
        self.__key = key
        self.__value = value

    def setValue(self, newValue):
        """Change the value of an item."""
        self.__value = newValue

    def value(self):
        """Get the value of an item."""
        return self.__value

    def key(self):
        """Get the key of an item."""
        return self.__key

class HashTable:
    """A simple Hash Table."""

    ## Initialisation

    def __init__(self):
        """Initialise a brand new hash table."""
        initial_size = 4
        self.__initialise_with_size(initial_size)

    def __initialise_with_size(self, size):
        """Make a new, completely empty hash-table with `size' buckets."""
        self.__hash_table = []
        # Make the right number of empty buckets.
        for each in range(0, size):
            bucket = []
            self.__hash_table.append(bucket)

    ## User-facing operations: get, put delete.

    def get(self, key):
        """Retrieve the value corresponding to `key'."""
        item = self.__get(key)
        if item:
            return item.value()
        return None

    def put(self, key, value):
        """Store a value, `value', for `key'."""
        existing_item = self.__get(key)
        if existing_item:
            # Update the value if we already have something for the key
            existing_item.setValue(value)
        else:
            # We don't already have something for this key.  Grow the hash table if needed...
            self.__maybe_expand()
            # ... and then add it to the bucket
            bucket = self.__bucket_for_key(key)
            bucket.append(BucketItem(key, value))

    def delete(self, key):
        """Delete the value for `key'"""
        # Check the item exists, and if so, get it.
        item = self.__get(key)
        if item:
            # Find the bucket its in, and remove it from the bucket.
            bucket = self.__bucket_for_key(key)
            bucket.remove(item)
            # Finally, check if we need to shrink the hash table.
            self.__maybe_shrink()

    ## Helper methods

    def __number_of_items(self):
        """Return the total number of items in the hash table."""
        # List comprehension. For each bucket, get the size of the bucket.
        bucket_lengths = [len(bucket) for bucket in self.__hash_table]
        # The number of items in the hash table is the number of items in all of the buckets.
        return sum(bucket_lengths)

    def __bucket_for_key(self, key):
        """Return the bucket for `key'."""
        # Hash it, and take the hash modulo the size of the hash table to find the index.
        idx = hash(key) % len(self.__hash_table)
        # Return bucket at that index
        return self.__hash_table[idx]

    def __get(self, key):
        """Internal implementation of 'get'.  Returns a BucketItem."""
        # Check the key of each item in the bucket
        for item in self.__bucket_for_key(key):
            if item.key() == key:
                # If we find the item, return it
                return item
        # If we don't find the item in the bucket, we return None to indicate that.
        return None

    ## Growing and shrinking, by copying everything to a new hash table

    def __resize(self, new_size):
        """Create a new hash table of `new_size', and re-populate it with the data from the old hash table."""
        old_hash_table = self.__hash_table
        self.__initialise_with_size(new_size)
        # Go into each bucket in the old hash table, and copy each item into the new hash table.
        for bucket in old_hash_table:
            for item in bucket:
                # We call `put' again for the item.  By doing this it will
                # re-distribute all of the items across the new hash table.
                self.put(item.key(), item.value())

    def __maybe_expand(self):
        """Double the size after crossing a certain threshold."""
        num_buckets = len(self.__hash_table)
        threshold = num_buckets
        # Grow if we have more items than buckets
        if self.__number_of_items() >= threshold:
            new_size = num_buckets * 2
            self.__resize(new_size)

    def __maybe_shrink(self):
        """Halve the size after crossing a certain threshold."""
        num_buckets = len(self.__hash_table)
        threshold = int(num_buckets / 2)
        # Shrink if we have less than half the number of items as buckets
        if self.__number_of_items() < threshold:
            new_size = int(num_buckets / 2)
            self.__resize(new_size)

    ## Hash table display format. Not required.

    def __str__(self):
        result = "{\n"
        for idx, bucket in enumerate(self.__hash_table):
            result += "  " + str(idx) + ": ["
            result += ", ".join([item.key() + ": " + item.value() for item in bucket])
            result += "]\n"
        result += "}"
        return result

### Create a new hash table and play with it, so we can see what happens.

if __name__ == '__main__':

    hashTable = HashTable()
    print("Empty hash table:")
    print(hashTable)
    print("\nAdd Jane:")
    hashTable.put("Jane", "98765432")
    print(hashTable)
    print("\nAdd 3 more people:")
    hashTable.put("John", "00000000")
    hashTable.put("Chris", "12345678")
    hashTable.put("Heidi", "23456789")
    print(hashTable)
    print("\nAdd a fifth person, and see everything reorganise:")
    hashTable.put("Fred", "45678901")
    print(hashTable)

    print("\nGet Heidi's phone number:")
    print(hashTable.get("Heidi"))
    print("\nChange Heidi's phone number:")
    hashTable.put("Heidi", "99999999")
    print(hashTable.get("Heidi"))
    print("\nGet Andy's phone number:")
    print(hashTable.get("Andy"))

    print("\nGet Jane's phone number:")
    print(hashTable.get("Jane"))
    print("\nDelete Jane's and John's phone numbers...")
    hashTable.delete("Jane")
    hashTable.delete("John")
    print("\nFinal hash table:")
    print(hashTable)

# ~~ Example Output ~~

# Empty hash table:
# {
#   0: []
#   1: []
#   2: []
#   3: []
# }
#
# Add Jane:
# {
#   0: []
#   1: []
#   2: [Jane: 98765432]
#   3: []
# }
#
# Add 3 more people:
# {
#   0: []
#   1: [John: 00000000, Heidi: 23456789]
#   2: [Jane: 98765432]
#   3: [Chris: 12345678]
# }
#
# Add a fifth person, and see everything reorganise:
# {
#   0: []
#   1: []
#   2: []
#   3: [John: 00000000, Fred: 45678901]
#   4: [Jane: 98765432]
#   5: [Chris: 12345678]
#   6: [Heidi: 23456789]
#   7: []
# }
#
# Get Heidi's phone number:
# 23456789
#
# Change Heidi's phone number:
# 99999999
#
# Get Andy's phone number:
# None
#
# Get Jane's phone number:
# 98765432
#
# Delete Jane's and John's phone numbers
#
# Final hash table:
# {
#   0: []
#   1: [Chris: 12345678]
#   2: [Heidi: 99999999]
#   3: [Fred: 45678901]
# }
