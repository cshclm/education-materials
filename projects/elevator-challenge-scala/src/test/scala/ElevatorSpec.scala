import org.scalatest._

import elevator._;

class ElevatorSpec extends FlatSpec with Matchers {
  "Elevator" should "start on the first floor" in {
    val e = Elevator()

    assert(e.thisFloor == 1)
  }

  it should "respond to a request to move up" in {
    val e = Elevator()
      .call(5, Down())
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "pick up someone on the way down" in {
    val e = Elevator()
      .call(5, Down())
      .runUntilStopped
      .selectFloor(1)
      .call(3, Down())
      .runUntilStopped

    assert(e.thisFloor == 3)
  }

  it should "continue after being interrupted" in {
    val e = Elevator()
      .call(5, Down())
      .runUntilStopped
      .selectFloor(1)
      .call(3, Down())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 1)
  }

  it should "always give priority to the current direction" in {
    val e = Elevator()
      .call(3, Down())
      .selectFloor(5)
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "respect floor requests in the opposite direction" in {
    val e = Elevator()
      .call(3, Down())
      .selectFloor(5)
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 3)
  }

  it should "continue in the current direction despite nearer requests" in {
    val e = Elevator()
      .selectFloor(3)
      .selectFloor(5)
      .runUntilStopped
      .selectFloor(2)
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "ignore in-elevator requests for the wrong direction" in {
    val e = Elevator()
      .selectFloor(3)
      .selectFloor(5)
      .runUntilStopped
      .selectFloor(2)
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "respect in-elevator requests in opposite direction once stopped" in {
    val e = Elevator()
      .selectFloor(3)
      .selectFloor(5)
      .runUntilStopped
      .runUntilStopped
      .selectFloor(2)
      .runUntilStopped

    assert(e.thisFloor == 2)
  }

  it should "change direction for floor requests" in {
    val e = Elevator()
      .call(2, Down())
      .call(4, Up())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 2)
  }

  it should "respect wait twice for simultaneous direction requests on a floor" in {
    val e = Elevator()
      .selectFloor(5)
      .call(5, Up())
      .call(5, Down())
      .runUntilStopped
      .selectFloor(4)   // Ignored
      .runUntilStopped
      .selectFloor(6)   // Ignored
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "service in-elevator requests made on-route as it reaches those floors" in {
    val e = Elevator()
      .selectFloor(6)
      .runUntilFloor(2)
      .selectFloor(3)
      .runUntilStopped

    assert(e.thisFloor == 3)
  }

  it should "service floor requests made on-route as it reaches those floors" in {
    val e = Elevator()
      .selectFloor(6)
      .runUntilFloor(2)
      .call(3, Up())
      .runUntilStopped

    assert(e.thisFloor == 3)
  }

  it should "ignore in-elevator requests made on-route which it has already passed" in {
    val e = Elevator()
      .selectFloor(6)
      .runUntilFloor(3)
      .selectFloor(2)
      .runUntilStopped

    assert(e.thisFloor == 6)
  }

  it should "queue floor requests made on-route which it has already passed" in {
    val e = Elevator()
      .selectFloor(6)
      .runUntilFloor(3)
      .call(2, Up())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 2)
  }

  it should "go idle if there are no subsequent directions" in {
    val e = Elevator()
      .call(5, Up())
      .runUntilStopped
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "go to two different floors in the correct order" in {
    val e = Elevator()
      .call(3, Up())
      .call(5, Up())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "not care about which order the floors are specified" in {
    val e = Elevator()
      .call(5, Up())
      .call(3, Up())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "go to the uppermost floor before going back down (1/2)" in {
    val e = Elevator()
      .call(3, Down())
      .call(5, Down())
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "go to the uppermost floor before going back down (2/2)" in {
    val e = Elevator()
      .call(3, Down())
      .call(5, Down())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 3)
  }

  it should "go to the floors in order even if called with opposite directions" in {
    val e = Elevator()
      .call(3, Up())
      .call(5, Down())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "go to the floors in order even if called with opposite directions and the first floor is further" in {
    val e = Elevator()
      .call(3, Down())
      .call(5, Up())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 3)
  }

  it should "go to the first floor it was requested, all else being equal (1/2)" in {
    val e = Elevator()
      .selectFloor(3)
      .runUntilStopped
      .call(2, Up())
      .call(4, Up())
      .runUntilStopped

    assert(e.thisFloor == 2)
  }

  it should "go to the first floor it was requested, all else being equal (2/2)" in {
    val e = Elevator()
      .selectFloor(3)
      .runUntilStopped
      .call(4, Up())
      .call(2, Up())
      .runUntilStopped

    assert(e.thisFloor == 4)
  }

  it should "allow requests to be made while moving (1/2)" in {
    val e = Elevator()
      .call(5, Up())
      .runUntilFloor(2)
      .call(3, Up())
      .runUntilStopped

    assert(e.thisFloor == 3)
  }

  it should "allow requests to be made while moving (2/2)" in {
    val e = Elevator()
      .call(5, Up())
      .runUntilFloor(2)
      .call(3, Up())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "come back to people who call the elevator after the doors close (1/2)" in {
    val e = Elevator()
      .call(5, Up())
      .runUntilFloor(3)
      .call(3, Up())
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "come back to people who call the elevator after the doors close (2/2)" in {
    val e = Elevator()
      .call(5, Up())
      .runUntilFloor(3)
      .call(3, Up())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 3)
  }

  it should "go to selected floors in linear order" in {
    val e = Elevator()
      .selectFloor(3)
      .selectFloor(5)
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "go to selected floors in linear order, regardless of order specified" in {
    val e = Elevator()
      .selectFloor(5)
      .selectFloor(3)
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "ensure floors selected in opposite directions causes only first to be respected" in {
  val e = Elevator()
      .selectFloor(3)
      .runUntilStopped
      .selectFloor(2)
      .selectFloor(4)
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 2)
  }

  it should "ensure floors selected in opposite directions causes only first to be respected, regardless of location" in {
  val e = Elevator()
      .selectFloor(3)
      .runUntilStopped
      .selectFloor(4)
      .selectFloor(2)
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 4)
  }

  it should "ignore silly users who say they want to go up, then try to go down (1/2)" in {
    val e = Elevator()
      .call(5, Down())
      .runUntilStopped
      .selectFloor(6)
      .selectFloor(4)
      .runUntilStopped

    assert(e.thisFloor == 4)
  }

  it should "ignore silly users who say they want to go up, then try to go down (2/2)" in {
    val e = Elevator()
      .call(5, Down())
      .runUntilStopped
      .selectFloor(6)
      .selectFloor(4)
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 4)
  }

  it should "not double-up on a floor it's selected to and called to (1/2)" in {
    val e = Elevator()
      .call(5, Down())
      .selectFloor(5)
      .runUntilStopped
      .selectFloor(4)
      .runUntilStopped

    assert(e.thisFloor == 4)
  }

  it should "not double-up on a floor it's selected to and called to (2/2)" in {
    val e = Elevator()
      .call(5, Down())
      .selectFloor(5)
      .runUntilStopped
      .selectFloor(4)
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 4)
  }

  it should "ignore repeated calls from the same floor (1/2)" in {
    val e = Elevator()
      .call(3, Up())
      .runUntilStopped
      .call(3, Up())
      .call(5, Down())
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "ignore repeated calls from the same floor (2/2)" in {
    val e = Elevator()
      .call(3, Up())
      .runUntilStopped
      .call(3, Up())
      .call(5, Down())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 5)
  }

  it should "lazily change directions" in {
    val e = Elevator()
      .call(2, Down())
      .call(4, Up())
      .runUntilStopped
      .call(5, Down())
      .runUntilStopped
      .runUntilStopped

    assert(e.thisFloor == 2)
  }

  it should "wait one step before changing directions" in {
    val e = Elevator()
      .selectFloor(5)
      .call(5, Down())
      .call(5, Up())
      .runUntilStopped
      .selectFloor(4)
      .runUntilStopped
      .selectFloor(6)  // Ignored
      .selectFloor(4)
      .runUntilStopped

    assert(e.thisFloor == 4)
  }

  it should "wait one step before changing directions, regardless of direction" in {
    val e = Elevator()
      .selectFloor(6)
      .runUntilStopped
      .selectFloor(2)
      .call(2, Up())
      .call(2, Down())
      .runUntilStopped
      .selectFloor(3)
      .runUntilStopped
      .selectFloor(1) // Ignored
      .selectFloor(3)
      .runUntilStopped

    assert(e.thisFloor == 3)
  }

  it should "come back if the direction has not been cleared" in {
    val e = Elevator()
      .selectFloor(5)
      .call(5, Up())
      .call(5, Down())
      .runUntilStopped
      .selectFloor(6)
      .runUntilStopped
      .runUntilStopped
      .selectFloor(6)
      .selectFloor(4)
      .runUntilStopped

    assert(e.thisFloor == 4)
  }

  it should "be able to go to basement levels" in {
    val e = Elevator()
      .selectFloor(-5)
      .runUntilStopped

    assert(e.thisFloor == -5)
  }

  it should "be able to go to arbitrarily high floors" in {
    val e = Elevator()
      .selectFloor(1000)
      .runUntilStopped

    assert(e.thisFloor == 1000)
  }
}
