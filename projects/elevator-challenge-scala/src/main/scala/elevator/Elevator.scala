package elevator

trait Direction
trait StopState extends Direction
trait FloorDirection extends Direction
case class Idle() extends StopState
case class Down() extends FloorDirection
case class Up() extends FloorDirection

case class State(val floor: Int, val direction: Direction)

trait ElevatorLogic {
  def step(state: State): State
  def selectFloor(target: State, current: State): Option[Direction]
  def call(target: State, current: State): Option[Direction]
}

// Implement me.
class ElevatorSimulator extends ElevatorLogic {
  def step(state: State): State = ???
  def selectFloor(target: State, current: State): Option[Direction] = ???
  def call(target: State, current: State): Option[Direction] = ???
}

object Elevator {
  class ElevatorImpl(floor: Int, direction: Direction, logic: ElevatorLogic) {
    def step: ElevatorImpl = {
      val s = logic.step(State(floor, direction))
      new ElevatorImpl(s.floor, s.direction, logic)
    }

    def thisFloor: Int = floor
    def thisDirection: Direction = direction
    def runUntilStopped: ElevatorImpl = step.untilStopped
    def runUntilFloor(floor: Int): ElevatorImpl = step.untilFloor(floor)

    def selectFloor(setFloor: Int): ElevatorImpl =
      logic.selectFloor(State(setFloor, direction), State(floor, direction)).map {
        new ElevatorImpl(floor, _, logic)
      } getOrElse this

    def call(setFloor: Int, setDirection: Direction): ElevatorImpl =
      logic.call(State(setFloor, setDirection), State(floor, direction)).map {
        new ElevatorImpl(floor, _, logic)
      } getOrElse this

    private def untilStopped: ElevatorImpl =
      if (direction.isInstanceOf[StopState]) this
      else step.untilStopped

    private def untilFloor(tf: Int): ElevatorImpl =
      if (floor == tf) this
      else step.untilFloor(tf)
  }

  def apply(): ElevatorImpl = new ElevatorImpl(1, Idle(), new ElevatorSimulator())
}
