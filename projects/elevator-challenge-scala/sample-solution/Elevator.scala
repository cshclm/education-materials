package elevator

trait Direction
trait StopState extends Direction
trait FloorDirection extends Direction

case class DoorOpen(d: FloorDirection) extends StopState
case class Exit() extends Direction
case class Idle() extends StopState
case class Down() extends FloorDirection
case class Up() extends FloorDirection

case class State(val floor: Int, val direction: Direction)

trait ElevatorLogic {
  def step(state: State): State
  def selectFloor(target: State, current: State): Option[Direction]
  def call(target: State, current: State): Option[Direction]
}

case class Floor(val up: Boolean = false, val down: Boolean = false, val exit: Boolean = false, val pending: Option[FloorDirection] = None) {
  def enable(d: Direction): Floor = set(d, true)
  def disable(d: Direction): Floor = set(d, false).set(Exit(), false)

  def get(d: Direction) = d match {
    case Up() => up
    case Down() => down
    case Exit() => exit
  }

  def applyPending(): Floor = pending.map {
    _ match {
      case Up() => new Floor(true, down, exit, None)
      case Down() => new Floor(up, true, exit, None)
      case _ => this
    }
  } getOrElse this

  def setPending(d: FloorDirection) = new Floor(up, down, exit, Some(d))

  private def set(d: Direction, state: Boolean): Floor = d match {
    case Up() => new Floor(state, down, exit, pending)
    case Down() => new Floor(up, state, exit, pending)
    case Exit() => new Floor(up, down, state, pending)
  }
}

class FloorManager {
  val floors = scala.collection.mutable.HashMap.empty[Int, Floor]

  def get(f: Int) = floors.get(f) getOrElse initFloor(f)

  def applyPending(f: Int): Unit =
    floors(f) = floors(f).applyPending()

  def setAfter(f: Int, d: Direction) =
    if (d.isInstanceOf[FloorDirection]) {
      val dc: FloorDirection = d.asInstanceOf[FloorDirection]
      set(f, _.setPending(dc))
    } else throw new RuntimeException("Unable to set pending state for " + d)

  def set(f: Int, m: Floor => Floor): Unit =
    if (floors.contains(f)) {
      floors(f) = m(floors(f))
    } else {
      initFloor(f)
      set(f, m)
    }

  def activeAny =
    floors.filter { case (i,f) => (f.exit || f.up || f.down) }

  def activeInElevator =
    floors.filter { case (i,f) => (f.exit) }

  def activeOnFloor =
    floors.filter { case (i,f) => (f.up || f.down) }

  def activeDirection(direction: Direction) = direction match {
    case Up() => activeAbove _
    case Down() => activeBelow _
  }

  def activeAbove(floor: Int) =
    floors.filter { case (i,f) => (f.up || f.exit || f.down) && i > floor }

  def activeBelow(floor: Int) =
    floors.filter { case (i,f) => (f.up || f.down || f.exit) && i < floor }

  private def initFloor(floor: Int): Floor = {
    val nf = new Floor();
    floors += (floor -> nf);
    nf
  }
}

class CoolElevator() extends ElevatorLogic {
  val fm = new FloorManager

  def step(state: State): State = {
    val ret = state.direction match {
      case DoorOpen(dir) => step(State(state.floor, dir))
      case Up() => doUp(state.floor)
      case Down() => doDown(state.floor)
      case Idle() => State(state.floor, Idle())
    }
    fm.applyPending(state.floor)
    ret
  }

  def call(target: State, current: State): Option[Direction] = {
    if (current.floor == target.floor) {
      if (target.direction == current.direction) fm.setAfter(target.floor, target.direction)
      return None
    }

    val floor = fm.get(target.floor)
    target.direction match {
      case Up() => fm.set(target.floor, _.enable(Up()))
      case Down() => fm.set(target.floor, _.enable(Down()))
    }

    if (target.floor > current.floor && current.direction == Idle()) Some(Up())
    else if (target.floor < current.floor && current.direction == Idle()) Some(Down())
    else None
  }

  def opComparison(dir: Direction): (Int, Int) => Boolean =
    dir match {
      case Up() => (_: Int) > (_: Int)
      case Down() => (_: Int) < (_: Int)
      case DoorOpen(d) => opComparison(d)
      case _ => (a, b) => false
    }

  def selectFloor(target: State, current: State): Option[Direction] = {
    if (opComparison(current.direction)(current.floor, target.floor)) return None

    fm.set(target.floor, _.enable(Exit()))
    if (target.floor > current.floor) Some(Up())
    else if (target.floor < current.floor) Some(Down())
    else None
  }

  def invert(direction: FloorDirection) = direction match {
    case Up() => Down()
    case Down() => Up()
  }

  private def doDirection(floor: Int, direction: FloorDirection) = {
    val explicitUp = fm.get(floor).get(direction)
    val explicitStop = fm.get(floor).get(Exit())
    fm.set(floor, _.disable(direction))
    if (explicitUp) {
      State(floor, DoorOpen(direction))
    } else if (fm.activeAny.isEmpty) {
      State(floor, Idle())
    } else if (explicitStop) {
      if (fm.activeDirection(direction)(floor).isEmpty) {
        fm.set(floor, _.disable(invert(direction)))
        State(floor, DoorOpen(invert(direction)))
      } else {
        State(floor, DoorOpen(direction))
      }
    } else if (fm.activeDirection(direction)(floor).nonEmpty) {
        direction match {
          case Up() => State(floor + 1, direction)
          case Down() => State(floor - 1, direction)
        }
    } else {
      doIdle(floor)
    }
  }

  private def doUp(floor: Int) = doDirection(floor, Up())
  private def doDown(floor: Int) = doDirection(floor, Down())

  private def doIdle(floor: Int): State = {
    val userActive = fm.activeInElevator
    val activeList = if (userActive.isEmpty) {
      fm.activeOnFloor
    } else {
      userActive
    }

    if (activeList.isEmpty) {
      State(floor, Idle())
    } else if (activeList.head._1 == floor) {
      val curFloor = fm.get(floor)
      if (curFloor.up) {
        fm.set(floor, _.disable(Up()))
        State(floor, DoorOpen(Up()))
      } else {
        fm.set(floor, _.disable(Down()))
        State(floor, DoorOpen(Down()))
      }
    } else if (activeList.head._1 > floor) {
      State(floor, Up())
    } else {
      State(floor, Down())
    }
  }
}

object Elevator {
  class ElevatorImpl(floor: Int, direction: Direction, logic: ElevatorLogic) {
    def step: ElevatorImpl = {
      val s = logic.step(State(floor, direction))
      new ElevatorImpl(s.floor, s.direction, logic)
    }

    def thisFloor: Int = floor
    def thisDirection: Direction = direction
    def runUntilStopped: ElevatorImpl = step.untilStopped
    def runUntilFloor(floor: Int): ElevatorImpl = step.untilFloor(floor)

    def selectFloor(setFloor: Int): ElevatorImpl =
      logic.selectFloor(State(setFloor, direction), State(floor, direction)).map {
        new ElevatorImpl(floor, _, logic)
      } getOrElse this

    def call(setFloor: Int, setDirection: Direction): ElevatorImpl =
      logic.call(State(setFloor, setDirection), State(floor, direction)).map {
        new ElevatorImpl(floor, _, logic)
      } getOrElse this

    private def untilStopped: ElevatorImpl =
      if (direction.isInstanceOf[StopState]) this
      else step.untilStopped

    private def untilFloor(tf: Int): ElevatorImpl =
      if (floor == tf) this
      else step.untilFloor(tf)
  }

  def apply(): ElevatorImpl = new ElevatorImpl(1, Idle(), new CoolElevator())
}
