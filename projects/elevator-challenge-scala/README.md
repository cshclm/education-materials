# Scala Elevator Challenge

## Description

This is a coding challenge to implement the logic for an Elevator Simulator in Scala.  It is based on the [Python Elevator Challenge](https://github.com/mshang/python-elevator-challenge/blob/master/README.md).  A suite of unit tests has been provided; the elevator logic implemented should pass all of these tests.

## Usage

The unit tests for the challenge can be ran using:

    sbt test

You are free to change any of the code provided, though the unit tests should not be modified.
